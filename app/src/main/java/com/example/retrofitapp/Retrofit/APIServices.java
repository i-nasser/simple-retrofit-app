package com.example.retrofitapp.Retrofit;

import com.example.retrofitapp.Models.CreateUserResponse;
import com.example.retrofitapp.Models.MultipleResource;
import com.example.retrofitapp.Models.User;
import com.example.retrofitapp.Models.UserList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.Url;

/****
 * Services
 * Apis Handling
 ****/

public interface APIServices {

    // Getting Any Data From Server Without Sending Any Params.
    @GET("/api/users?page=2")
    Call<MultipleResource> doGetListResources();

    // @Query -> Use It To Send Any Parameter Mandatory And Parameter Optional To Server Use GET & POST
    @GET("/api/users?")
    Call<UserList> doGetUserList(@Query("page") String page);

    /***************************************************************************************************/

    // @Field -> Use It To Sending Forms | Any Params And Requires a mandatory parameter Only with a POST
    @FormUrlEncoded
    @POST("/api/users?")
    Call<CreateUserResponse> doCreateUserWithField(@Field("name") String name, @Field("job") String job);

    // @Field -> Use It To Sending Forms | Any Params And Requires a mandatory parameter Only with a PUT
    @FormUrlEncoded
    @PUT("/api/users?")
    Call<CreateUserResponse> doCreateUserWithFieldPut(@Field("name") String name, @Field("job") String job);


    /***************************************************************************************************/

    // @Body Use it When Sends Java objects as request body.
    @POST("/api/users")
    Call<User> createUser(@Body User user);


    //@Url Use it -> When Need dynamic URLs.
    @POST("/api/users")
    Call<User> createUser2(@Url User user);
}
