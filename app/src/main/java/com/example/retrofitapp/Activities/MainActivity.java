package com.example.retrofitapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.example.retrofitapp.Models.CreateUserResponse;
import com.example.retrofitapp.Models.MultipleResource;
import com.example.retrofitapp.Models.UserList;
import com.example.retrofitapp.R;
import com.example.retrofitapp.Retrofit.APIClient;
import com.example.retrofitapp.Retrofit.APIServices;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TextView TVResponseText;
    private APIServices apiServices;
    private String displayResponse = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TVResponseText = findViewById(R.id.TVResponseText);
        apiServices = APIClient.getClient().create(APIServices.class);

        /**
         GET List Resources Without Params
         **/
        Call<MultipleResource> call = apiServices.doGetListResources();
        call.enqueue(new Callback<MultipleResource>() {
            @Override
            public void onResponse(Call<MultipleResource> call, Response<MultipleResource> response) {

                Log.d("TAG",response.code()+"");

                MultipleResource resource = response.body();

                Integer text = resource.getPage();
                Integer total = resource.getTotal();
                Integer totalPages = resource.getTotalPages();
                ArrayList<MultipleResource.dataModel> datumList = resource.getData();

                displayResponse += "\n******* Response 1 -> (GET) Without Param *******\n\n";

                displayResponse += text + " : Page\n" + total + " : Total\n" + totalPages + " : Total Pages\n\n";

                for (MultipleResource.dataModel dataModel : datumList) {
                    displayResponse += dataModel.id + " : id\n" + dataModel.email + " : email\n" + dataModel.first_name + " : first_name\n" + dataModel.last_name + " : last_name\n" + dataModel.avatar + " : avatar\n\n";
                }

                TVResponseText.setText(displayResponse);
            }
            @Override
            public void onFailure(Call<MultipleResource> call, Throwable t) {
                call.cancel();
            }
        });


        /**
         GET List Users By Params
         **/
        Call<UserList> call2 = apiServices.doGetUserList("2");
        call2.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {

                UserList userList = response.body();
                Integer text = userList.getPage();
                Integer total = userList.getTotal();
                Integer totalPages = userList.getTotalPages();
                ArrayList<UserList.Datum> datumList = userList.getData();

                displayResponse += "\n******* Response 2 -> (GET) With Param *******\n\n";

                displayResponse += text + " : Page\n" + total + " : Total\n" + totalPages + " : Total Pages\n\n";

                for (UserList.Datum dataModel : datumList) {
                    displayResponse += dataModel.id + " : id\n" + dataModel.first_name + " : first_name\n" + dataModel.last_name + " : last_name\n" + dataModel.avatar + " : avatar\n\n";
                }

                TVResponseText.setText(displayResponse);
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {
                call.cancel();
            }
        });


        /**
         POST name and job Url encoded.
         **/
        Call<CreateUserResponse> call3 = apiServices.doCreateUserWithField("morpheus","leader");
        call3.enqueue(new Callback<CreateUserResponse>() {
            @Override
            public void onResponse(Call<CreateUserResponse> call, Response<CreateUserResponse> response) {
                CreateUserResponse userList = response.body();
                String name = userList.getName();
                String job = userList.getJob();
                String id = userList.getId();
                String createdAt = userList.getCreatedAt();

                displayResponse += "\n******* Response 3 -> (POST) With Param *******\n\n";

                displayResponse += name + " : name\n" + job + " : job\n" + id + " : id\n" + createdAt + " : createdAt\n";

                TVResponseText.setText(displayResponse);
            }
            @Override
            public void onFailure(Call<CreateUserResponse> call, Throwable t) {
                call.cancel();
            }
        });


        /**
         PUT name and job Url encoded.
         **/
        Call<CreateUserResponse> call4 = apiServices.doCreateUserWithField("morpheus","zion resident");
        call4.enqueue(new Callback<CreateUserResponse>() {
            @Override
            public void onResponse(Call<CreateUserResponse> call, Response<CreateUserResponse> response) {
                CreateUserResponse userList = response.body();
                String name = userList.getName();
                String job = userList.getJob();
                String createdAt = userList.getCreatedAt();

                displayResponse += "\n******* Response 4 -> (PUT) With Param *******\n\n";

                displayResponse += name + " : name\n" + job + " : job\n" + createdAt + " : createdAt\n";

                TVResponseText.setText(displayResponse);
            }
            @Override
            public void onFailure(Call<CreateUserResponse> call, Throwable t) {
                call.cancel();
            }
        });

    }
}
